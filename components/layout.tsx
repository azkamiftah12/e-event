import {
    AppShell,
    Navbar,
    Header,
    Footer,
    Text,
    MediaQuery,
    Burger,
    useMantineTheme,
    Button,
  createStyles,
    Space,
    Grid,
    ScrollArea,
    ThemeIcon,
    Box,
  } from '@mantine/core';

import { useState } from 'react';
import { IconAdjustments, IconCalendarStats, IconFileAnalytics, IconGauge, IconLock, IconNotes, IconPresentationAnalytics } from '@tabler/icons-react';
import { LinksGroup } from './NavbarLinksGroup';

export const ipaddress = 'http://192.168.1.16:8081/';

export default function Layout({ children }) {
    const theme = useMantineTheme();
    
    // scrolldown menu start
  const [opened, setOpened] = useState(false);
  const mockdata = [
    {
      label: 'Internal',
      icon: IconNotes,
      initiallyOpened: false,
      links: [
        { label: 'Pelatihan', link: '/internal/pelatihan' },
        { label: 'Validasi Pelatihan', link: '/internal/validasipelatihan' },
        { label: 'Batch', link: '/internal/batch' },
      ],
    },
    {
      label: 'Manajemen',
      icon: IconAdjustments,
      links: [
        { label: 'Provinsi', link: '/manajemen/provinsi' },
        { label: 'Kabupaten', link: '/manajemen/kabupaten' },
        { label: 'Jenis Pekerjaan', link: '/manajemen/jenispekerjaan' },
        { label: 'Jenis Pelatihan', link: '/manajemen/jenispelatihan' },
        { label: 'Narasumber', link: '/manajemen/narasumber' },
        { label: 'User', link: '/manajemen/user' },
      ],
    },
  ];
  const links = mockdata.map((item) => <LinksGroup {...item} key={item.label} />);
  // scrolldown menu end

  // Dashboard clickable button start 
  const useStyles = createStyles((theme) => ({
    control: {
      fontWeight: 800,
      display: 'block',
      width: '100%',
      padding: `${theme.spacing.xs} ${theme.spacing.md}`,
      color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,
      fontSize: theme.fontSizes.md,
  
      '&:hover': {
        backgroundColor: theme.colorScheme === 'dark' ? theme.colors.dark[7] : theme.colors.gray[0],
        color: theme.colorScheme === 'dark' ? theme.white : theme.black,
      },
    },
  }));
  const { classes } = useStyles();
  // Dashboard clickable button end 

  return (
    <>
      <AppShell
        styles={{
        main: {
          background: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0],
        },
      }}
        navbarOffsetBreakpoint="sm"
        asideOffsetBreakpoint="sm"
        // navbar head start
        header={
            <Header height={{ base: 50, md: 70 }} p="md">
              <div style={{ display: 'flex', alignItems: 'center', height: '100%' }}>
                <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
                  <Burger
                    opened={opened}
                    onClick={() => setOpened((o) => !o)}
                    size="sm"
                    color={theme.colors.gray[6]}
                    mr="xl"
                  />
                </MediaQuery>
                <Text tt="uppercase" size="xl" fw={700}>E-Event</Text>
              </div>
            </Header>
          }
          // navbar head end
          // navbar side start
        navbar={
          <ScrollArea>
            <Navbar p="xs" hiddenBreakpoint="sm" hidden={!opened} width={{ sm: 200, lg: 250 }}>
          <Grid>
            <Grid.Col span={12}>
            <Button component="a" href="/" color="pink.9" radius="md" size="md" fullWidth>
                <Text size="lg" color="gray.0">Home</Text>
            </Button>
            </Grid.Col>
          
          </Grid>
            <Space h="xl" />

          <Navbar.Section grow component={ScrollArea} mx="-xs" px="xs">
            {/* dashboard button clickable start  */}
          <Text<'a'>
            component="a"
            className={classes.control}
            href="/admin"
            
          >
            <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <ThemeIcon variant="light" size={30}>
            <IconGauge size={18} />
            </ThemeIcon>
            <Box ml="md">Dashboard</Box>
            </Box>
          </Text>
          {/* dashboard button clickable End  */}
            {/* new button start  */}
          <div>{links}</div>
          {/* new button end  */}

          {/* old button start  */}
          {/* Old button put here  */}
            {/* old button end  */}
          </Navbar.Section>
            
            </Navbar>
          </ScrollArea>
        
      }
      //navbar side end
      // navbar footer start
        footer={
        <Footer height={60} p="md">
          <Text ta="center" size="xs" fw={100}>
          E-Event @Copyright TI-CCIT 6
          </Text>
        </Footer>
      }
      //navbar footer end
      > 
        <div>
        {children}
        </div> 
      </AppShell>
    </>
  );
}
