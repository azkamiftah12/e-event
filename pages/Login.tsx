import axios from 'axios';
import { SyntheticEvent, useState } from 'react';
import { Notification, TextInput, PasswordInput, Anchor, Paper, Title, Text, Container, Group, Button, Space } from '@mantine/core';
import { useRouter } from 'next/router';
import { useForm } from '@mantine/form';
import { IconX } from '@tabler/icons';
import { ipaddress } from '../components/layout';
import HeaderMenu from '../components/HeaderMenu/HeaderMenu';
import FooterMenu from '../components/FooterMenu/FooterMenu';
  
  const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const router = useRouter();

    //notification gagal start
    const [showNotificationdelete, setShowNotificationdelete] = useState(false);
    const handleCloseNotificationdelete = () => {
      setShowNotificationdelete(false);
    };
    //notification gagal end

    const pageStyle = {
        backgroundColor: '#E0DAD1',
      };

      //login start
      const handleLogin = async (e) => {
        e.preventDefault();
    
        try {
          // Send login request to your backend API
          const response = await axios.post(`${ipaddress}login`, { username, password });
    
          if (response.status === 200) {
      const { token } = response.data;

      // Save token to local storage or cookie
      localStorage.setItem('token', token);

      // Redirect to user profile page
      router.push('/admin');
    } else {
      console.log('Login error:', response.data.error);
      // Display error message
    }
  } catch (error) {
    setShowNotificationdelete(true);
    // Display error message
    }
      };
      //login end

    return (<>
    <div style={pageStyle}>
    <HeaderMenu />
      <Container size={470} my={108}>

      {showNotificationdelete && (
        <Notification
          icon={<IconX size="1.1rem" />}
          color="red"
          onClose={handleCloseNotificationdelete}
        >
          Gagal Login
        </Notification>
      )}

<Space h="xl" />

        <Title
          align="center"
          sx={(theme) => ({ fontFamily: `Greycliff CF, ${theme.fontFamily}`, fontWeight: 900, fontSize: '45px' })}
        >
          Welcome back!
        </Title>
        <Text color="dimmed" size="md" align="center" mt={5}>
          Belum punya akun?{' '}
          <Anchor size="sm" style={{ color: '#e14658' }} component="a" href="/SignUp">
            Sign Up
          </Anchor>
        </Text>
  
        <Paper withBorder shadow="md" p={30} mt={30} radius="md">
          <form onSubmit={handleLogin}>
          <TextInput
            label="Username"
            placeholder="Insert Username Here"
            value={username}
            onChange={e => setUsername(e.target.value)}
            required
          />
            <PasswordInput
              label="Password"
              placeholder="Your Password Here"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
              mt="md"
            />
            <Group position="apart" mt="lg">
              <Anchor component="button" size="sm" style={{ color: '#e14658' }}>
                Forgot password?
              </Anchor>
            </Group>
            <Button
              type="submit"
              fullWidth
              mt="xl"
              styles={(theme) => ({
                root: {
                  backgroundColor: '#e14658',
                  color: '#ffffff',
                  '&:not([data-disabled])': theme.fn.hover({
                    backgroundColor: '#e7b622',
                    color: theme.fn.darken('#3F2661', 0.15),
                  }),
                },
              })}
            >
              Sign in
            </Button>
          </form>
        </Paper>
      </Container>
      <FooterMenu />
    </div>
            </>
    );
  };
export default Login;
