import axios from 'axios';
import { useState, useEffect } from 'react';
import { Badge, Button, Card, Container, Grid, Group, Image, Space, Text, TextInput, Title } from '@mantine/core';
import HeaderMenu from '../components/HeaderMenu/HeaderMenu';
import { ipaddress } from '../components/layout';
import FooterMenu from '../components/FooterMenu/FooterMenu';

const latihan = () => {
    const [data, setData] = useState([]);
    const pageStyle = {
        backgroundColor: '#E0DAD1',
      };
    
    const getData = async () => {
        const response = await axios.get(`${ipaddress}get-datapelatihan`);
        console.log(response.data.data);
        setData(response.data.data);
      };
    
      useEffect(() => {
        getData();
      }, []);

      //search
  const [searchTerm, setSearchTerm] = useState('');
  const handleSearch = (event) => {
    setSearchTerm(event.target.value);
  };
  
  // eslint-disable-next-line arrow-body-style
  const filteredData = data.filter((item) => {
    return item.judul_pelatihan?.toString().toLowerCase().includes(searchTerm.toLowerCase());
  });
  //search end

      return (
        <div style={pageStyle}>
    <HeaderMenu />
    <Space h="xl" />
    <Title
      align="center"
      sx={(theme) => ({ fontFamily: `Greycliff CF, ${theme.fontFamily}`, fontWeight: 900, fontSize: '45px' })}
    >
          List Pelatihan
    </Title>
    <Container size="xl" px="xl">
    <Space h="xl" />

    <TextInput
      placeholder="search pelatihan"
      value={searchTerm}
      onChange={handleSearch}
      style={{ marginTop: '16px' }}
    />
      <Space h="xl" />

          {/* Card Start */}
    <Grid>
        {filteredData.map((e) => (
          <Grid.Col span={4}>
                  <Card key={e.id_pelatihan} shadow="sm" padding="lg" radius="md" withBorder>
                  <Card.Section>
                    <Image
                      src="https://images.unsplash.com/photo-1527004013197-933c4bb611b3?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=720&q=80"
                      height={160}
                      alt="Norway"
                    />
                  </Card.Section>
            
                  <Group position="apart" mt="md" mb="xs">
                    <Text weight={500}>{e.judul_pelatihan}</Text>
                    <Badge color="pink" variant="light">
                      On Sale
                    </Badge>
                  </Group>
            
                  <Text size="sm" color="dimmed">
                  {e.deskripsi_pelatihan}
                  </Text>
            
                  <Button variant="light" color="blue" fullWidth mt="md" radius="md">
                    Claim Pelatihan
                  </Button>
                  </Card>
          </Grid.Col>
        ))}
    </Grid>
        {/* Card End */}

    <Space h="xl" />
    </Container>
    <FooterMenu />
        </div>
      );
};
export default latihan;
