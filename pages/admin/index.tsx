import { Badge, Button, Card, Container, Grid, Group, Title, Image, Text, Space, RingProgress } from '@mantine/core';
import { useRouter } from 'next/router';
import Layout from '../../components/layout';

const index = () => {
    const router = useRouter();
    
    return (
        <Layout>
            <Title>
            Dashboard
            </Title>
            <Space h="xl" />
            <Container>
            <RingProgress
              size={330}
              thickness={24}
              roundCaps
              label={
        <Text size="xl" align="center">
          Jenis Pelatihan
        </Text>
      }
              sections={[
        { value: 40, color: 'cyan', tooltip: 'Pelatihan Memasak - 40' },
        { value: 15, color: 'orange', tooltip: 'Pelatihan Militer - 15' },
        { value: 15, color: 'grape', tooltip: 'Pelatihan Kesehatan - 15' },
        { value: 15, color: 'teal', tooltip: 'Pelatihan Komputer - 15' },
      ]}
            />

        <Space h="xl" />

                {/* Card Start */}
        <Grid>
          <Grid.Col span={4}>
                  <Card shadow="sm" padding="lg" radius="md" withBorder>
            
                  <Group position="apart" mt="md" mb="xs">
                    <Text weight={500}>
                        Pelatihan
                    </Text>
                    <Badge color="pink" variant="light">
                      Jumlah:
                    </Badge>
                  </Group>
            
                  <Button component="a" href="/internal/pelatihan" variant="light" color="blue" fullWidth mt="md" radius="md">
                    Detail
                  </Button>
                  </Card>
          </Grid.Col>
        
          <Grid.Col span={4}>
                  <Card shadow="sm" padding="lg" radius="md" withBorder>
                  <Group position="apart" mt="md" mb="xs">
                    <Text weight={500}>
                        Pelatihan perlu validasi
                    </Text>
                    <Badge color="pink" variant="light">
                      Jumlah:
                    </Badge>
                  </Group>
            
                  <Button component="a" href="/internal/validasipelatihan" variant="light" color="blue" fullWidth mt="md" radius="md">
                    Detail
                  </Button>
                  </Card>
          </Grid.Col>
     
          <Grid.Col span={4}>
                  <Card shadow="sm" padding="lg" radius="md" withBorder>
                  <Group position="apart" mt="md" mb="xs">
                    <Text weight={500}>
                        Batch
                    </Text>
                    <Badge color="pink" variant="light">
                      Jumlah:
                    </Badge>
                  </Group>
            
                  <Button component="a" href="/internal/batch" variant="light" color="blue" fullWidth mt="md" radius="md">
                    Detail
                  </Button>
                  </Card>
          </Grid.Col>
        {/* ))} */}
    
        {/* {filteredData.map((e) => ( */}
          <Grid.Col span={4}>
                  <Card shadow="sm" padding="lg" radius="md" withBorder>            
                  <Group position="apart" mt="md" mb="xs">
                    <Text weight={500}>
                        User
                    </Text>
                    <Badge color="pink" variant="light">
                      Jumlah:
                    </Badge>
                  </Group>
            
                  <Button component="a" href="/manajemen/user" variant="light" color="blue" fullWidth mt="md" radius="md">
                    Detail
                  </Button>
                  </Card>
          </Grid.Col>
        {/* ))} */}
        </Grid>
        {/* Card End */}
        
            </Container>
        </Layout>
    );
};
export default index;
