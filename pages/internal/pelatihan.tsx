import { Notification, ActionIcon, Anchor, Box, Select, Button, Grid, Group, Modal, Space, Table, Text, TextInput, CloseButton, Textarea } from '@mantine/core';
import axios from 'axios';
import React, { useState, useEffect, useRef } from 'react';
import { IconCalendar, IconCheck, IconClock, IconX } from '@tabler/icons-react';
import { useForm } from '@mantine/form';
import { useDisclosure } from '@mantine/hooks';
import { DatesProvider, MonthPickerInput, DatePickerInput, TimeInput } from '@mantine/dates';
import { modals } from '@mantine/modals';
import Layout, { ipaddress } from '../../components/layout';

const pelatihan = () => {
    const [data, setData] = useState([]);
    const ref = useRef<HTMLInputElement>();
    
    //notification delete start
    const [showNotificationdelete, setShowNotificationdelete] = useState(false);
    const handleCloseNotificationdelete = () => {
      setShowNotificationdelete(false);
    };
    //notification delete end
    
    //notification create start
    const [showNotificationcreate, setShowNotificationcreate] = useState(false);
    const handleCloseNotificationcreate = () => {
      setShowNotificationcreate(false);
    };
    //notification create end
    
    const getData = async () => {
      const response = await axios.get(`${ipaddress}get-datapelatihan`);
      console.log(response.data.data);
      setData(response.data.data);
    };
    
    const getDataJenisacara = async () => {
      const response = await axios.get(`${ipaddress}get-dataacara`);
      console.log(response.data.data);
      const temporaryData = response.data.data.map(v => ({ label: v.nama_jenis_acara, value: v.id_jenis_acara }));
      setDataJenisacara(temporaryData);
    };
    
    const getDataNarasumber = async () => {
      const response = await axios.get(`${ipaddress}get-datanarasumber`);
      console.log(response.data.data);
      const temporaryData = response.data.data.map(v => ({ label: v.nama_narasumber, value: v.id_narasumber }));
      setDataNarasumber(temporaryData);
    };

    useEffect(() => {
      getData();
      getDataJenisacara();
      getDataNarasumber();
    }, []);
    
    //search
    const [searchTerm, setSearchTerm] = useState('');
    const handleSearch = (event) => {
      setSearchTerm(event.target.value);
    };
    
    // eslint-disable-next-line arrow-body-style
    const filteredData = data.filter((item) => {
      return item.judul_pelatihan?.toString().toLowerCase().includes(searchTerm.toLowerCase());
    });
    //search end
    
    //modal start
  const [opened, { open, close }] = useDisclosure(false);
  // modal end
  
  //form start
  const form = useForm({
    initialValues: {
        id_jenis_acara: '',
        judul_pelatihan: '',
        deskripsi_pelatihan: '',
        nama_narasumber: '',
        tanggal_pelatihan_start: new Date(),
        tanggal_pelatihan_end: '',
        waktu_pelatihan: '',
        link_pelatihan: '',
        max_pesertabatch: '',
      },
    validate: {
      id_jenis_acara: (value) => (value.length < 1 ? 'Please Fill This!' : null),
      judul_pelatihan: (value) => (value.length < 2 ? 'Please Fill This!' : null),
      deskripsi_pelatihan: (value) => (value.length < 2 ? 'Please Fill This!' : null),
      nama_narasumber: (value) => (value.length < 2 ? 'Please Fill This!' : null),
      // tanggal_pelatihan: (value) => (value.length < 2 ? 'Please Fill This!' : null),
      // waktu_pelatihan: (value) => (value.length < 2 ? 'Please Fill This!' : null),
      link_pelatihan: (value) => (value.length < 2 ? 'Please Fill This!' : null),
      max_pesertabatch: (value) => (value.length < 2 ? 'Please Fill This!' : null),
    },
  });
  //Form End
  
  const [searchValue, onSearchChange] = useState('');

  const [dataJenisacara, setDataJenisacara] = useState([]);
  
  const [dataNarasumber, setDataNarasumber] = useState([]);
  const [searchValueNarasumber, onSearchChangeNarasumber] = useState('');
  //Insert
  const handleInsert = async () => {
    const { id_jenis_acara } = form.values;
    const { judul_pelatihan } = form.values;
    const { deskripsi_pelatihan } = form.values;
      const selectedOptionNarasumber = dataNarasumber.find((option) => option.label === searchValueNarasumber);
      const id_narasumber = selectedOptionNarasumber ? selectedOptionNarasumber.value : '';
      const { waktu_pelatihan } = form.values;
      const { link_pelatihan } = form.values;
      const { max_pesertabatch } = form.values;
      
      const dateMaster1 = new Date(form.values.tanggal_pelatihan_start);
    const timezoneOffset1 = dateMaster1.getTimezoneOffset() * 60000; // Convert minutes to milliseconds
    const adjustedDate1 = new Date(dateMaster1.getTime() - timezoneOffset1);
    const tanggal_pelatihan_start = adjustedDate1.toISOString().split('T')[0];
    
    const dateMaster = new Date(form.values.tanggal_pelatihan_end);
  const timezoneOffset = dateMaster.getTimezoneOffset() * 60000; // Convert minutes to milliseconds
const adjustedDate = new Date(dateMaster.getTime() - timezoneOffset);
  const tanggal_pelatihan_end = adjustedDate.toISOString().split('T')[0];

    const bodyFormData = new FormData();
    bodyFormData.append('id_jenis_acara', id_jenis_acara);
    bodyFormData.append('judul_pelatihan', judul_pelatihan);
    bodyFormData.append('deskripsi_pelatihan', deskripsi_pelatihan);
    bodyFormData.append('id_narasumber', id_narasumber);
    bodyFormData.append('tanggal_pelatihan_start', tanggal_pelatihan_start);
    bodyFormData.append('tanggal_pelatihan_end', tanggal_pelatihan_end);
    bodyFormData.append('waktu_pelatihan', waktu_pelatihan);
    bodyFormData.append('link_pelatihan', link_pelatihan);
    bodyFormData.append('max_pesertabatch', max_pesertabatch);
    
    try {
      await axios.post(`${ipaddress}insert-datapelatihan`, bodyFormData, {
        headers: { 'Content-Type': 'multipart/form-data' },
      });
      close(false);
      setShowNotificationdelete(false);
      setShowNotificationcreate(true);
      getData();
    } catch (error) {
      // Handle the error
      console.error(error);
    }
  };
  //insert end
  
  //delete
  const handleDelete = async (id_pelatihan) => {
    const bodyFormData = new FormData();
  console.log(id_pelatihan);
  bodyFormData.append('idpelatihan', id_pelatihan);
  await axios.post(`${ipaddress}delete-datapelatihan/:${id_pelatihan}`, bodyFormData, {
    headers: { 'Content-Type': 'multipart/form-data' },
  });
  setShowNotificationdelete(true);
  setShowNotificationcreate(false);
  getData();
};
//delete end

//open model delete start
const openDeleteModal = (e) => {
  modals.openConfirmModal({
    title: 'Delete your profile',
    centered: true,
    children: (
      <Text size="sm">
        Are you sure you want to delete <strong>{e.judul_pelatihan}?</strong>
      </Text>
    ),
    labels: { confirm: 'Delete Pelatihan', cancel: 'Cancel' },
    confirmProps: { color: 'red' },
    onCancel: () => console.log('Cancel'),
    onConfirm: () => handleDelete(e.id_pelatihan),
  });
  };
    //open model delete end
    
  // datetable parse start
  const formatdatepelatihan = (sampletanggal) => {
    // const sampletanggal = '2023-05-21T00:00:00Z';
    if (sampletanggal === '' || sampletanggal == null || sampletanggal === undefined) {
      return '';
    }
    const parsedDate = new Date(sampletanggal);
    return parsedDate.toISOString().split('T')[0];
  };
  // datetable parse end

  // timetable parse start
  const formattimepelatihan = (sampletime) => {
    // const sampletanggal = '2023-05-21T00:00:00Z';
    if (sampletime === '' || sampletime == null || sampletime === undefined) {
      return '';
    }
    const parsedTime = new Date(sampletime);
    const formattedTime = parsedTime.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit', hour12: false });
return formattedTime;
  };
  // timetable parse end

      return (
  <Layout>
    {showNotificationcreate && (
      <Notification
        icon={<IconCheck size="1.1rem" />}
        color="teal"
        title="Notification"
        onClose={handleCloseNotificationcreate}
      >
        Pelatihan berhasil diinput. Tunggu untuk verifikasi pelatihan
      </Notification>
    )}

    {showNotificationdelete && (
        <Notification
          icon={<IconCheck size="1.1rem" />}
          color="red"
          onClose={handleCloseNotificationdelete}
        >
          Data berhasil dihapus
        </Notification>
      )}

    <Modal size="70%" opened={opened} onClose={close} title="Add pelatihan" centered>
        <Box my="lg" mx="auto" maw="70%">
          <form onSubmit={form.onSubmit((values) => console.log(values))}>
          <Grid>
              <Grid.Col span={5} mx="lg">
            <Select
              label="Jenis Acara"
              placeholder="Pilih Acara"
              searchable
              onSearchChange={onSearchChange}
              searchValue={searchValue}
              nothingFound="No options"
              data={dataJenisacara}
              {...form.getInputProps('id_jenis_acara')}
            />              
            
            <Space h="md" />
            <TextInput
              withAsterisk
              label="Judul Pelatihan"
              placeholder="Judul Pelatihan"
              {...form.getInputProps('judul_pelatihan')}
            />
            <Space h="md" />
            <Textarea
              withAsterisk
              label="Deskripsi Pelatihan"
              placeholder="Deskripsi Pelatihan"
              {...form.getInputProps('deskripsi_pelatihan')}
            />

            <Space h="md" />

            <Select
              label="Narasumber"
              placeholder="Pilih Narasumber"
              searchable
              onSearchChange={onSearchChangeNarasumber}
              searchValue={searchValueNarasumber}
              nothingFound="No options"
              data={dataNarasumber}
              {...form.getInputProps('id_narasumber')}
            />
            
            <Space h="md" />

              </Grid.Col>
              <Grid.Col span={5} mx="lg">
              <DatesProvider settings={{ firstDayOfWeek: 1, weekendDays: [0] }}>
            <DatePickerInput
              icon={<IconCalendar size="1.1rem" stroke={1.5} />}
              label="Pilih Tanggal Mulai pelatihan"
              placeholder="Pilih Tanggal Mulai pelatihan"
              {...form.getInputProps('tanggal_pelatihan_start')}
              mx="auto"
              maw={400}
            />
              </DatesProvider>

            <Space h="md" />

            <DatesProvider settings={{ firstDayOfWeek: 1, weekendDays: [0] }}>
            <DatePickerInput
              icon={<IconCalendar size="1.1rem" stroke={1.5} />}
              label="Pilih Tanggal Akhir Pelatihan"
              placeholder="Pilih Tanggal akhir pelatihan"
              {...form.getInputProps('tanggal_pelatihan_end')}
              mx="auto"
              maw={400}
            />
            </DatesProvider>

            <Space h="md" />

            <TimeInput
              label="Pilih Jam Pelatihan"
              ref={ref}
              withSeconds
              rightSection={
        <ActionIcon onClick={() => ref.current.showPicker()}>
          <IconClock size="1rem" stroke={1.5} />
        </ActionIcon>
      }
              {...form.getInputProps('waktu_pelatihan')}
              maw={400}
              mx="auto"
            />
            <Space h="md" />
            <TextInput
              withAsterisk
              label="Link Pelatihan"
              placeholder="Link Pelatihan"
              {...form.getInputProps('link_pelatihan')}
            />
            <Space h="md" />
            <TextInput
              withAsterisk
              label="Max Peserta per Batch"
              placeholder="Max Peserta per Batch"
              {...form.getInputProps('max_pesertabatch')}
            />
              </Grid.Col>
          </Grid>

            <Group position="right" mt="md">
              <Button type="submit" onClick={handleInsert}>Submit</Button>
            </Group>
            {/* <Button onClick={dateparse}>test date</Button>
            <Button onClick={timeparse}>test time</Button> */}
          </form>
        </Box>
    </Modal>
    <Space h="md" />
      <Group position="center">
        <Button color="indigo" onClick={open}>Add Pelatihan</Button>
      </Group>

      <TextInput
        placeholder="Search pelatihan"
        value={searchTerm}
        onChange={handleSearch}
        style={{ marginTop: '16px' }}
      />

<Space h="md" />
        <Table striped highlightOnHover withBorder withColumnBorders>
      <thead>
        <tr>
          <th>Judul Pelatihan</th>
          <th>Deskripsi Pelatihan</th>
          <th>Nama Narasumber</th>
          <th>Tanggal Pelatihan Mulai</th>
          <th>Tanggal Pelatihan Akhir</th>
          <th>Jam Pelatihan</th>
          <th>Link Pelatihan</th>
          <th>Max Peserta per Batch</th>
          <th>Username ACC pelatihan</th>
          <th>Tanggal ACC Pelatihan</th>
          <th>Waktu ACC Pelatihan</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
      {filteredData.map((e) => (
            <tr key={e.id_pelatihan}>
              <td>{e.judul_pelatihan}</td>
              <td>{e.deskripsi_pelatihan}</td>
              <td>{e.nama_narasumber}</td>
              <td>{formatdatepelatihan(e.tanggal_pelatihan_start)}</td>
              <td>{formatdatepelatihan(e.tanggal_pelatihan_end)}</td>
              <td>{formattimepelatihan(e.waktu_pelatihan)}</td>
              <td><Anchor href={e.link_pelatihan} target="_blank">
              {e.link_pelatihan}
                  </Anchor>
              </td>
              <td>{e.max_pesertabatch}</td>
              <td>{e.username_acc}</td>
              <td>{formatdatepelatihan(e.tanggal_pelatihan_acc)}</td>
              <td>{formattimepelatihan(e.waktu_pelatihan_acc)}</td>
              <td>
                <Button onClick={() => openDeleteModal(e)} color="red">
                  Delete
                </Button>
              </td>
            </tr>
          ))}
      </tbody>
        </Table>
  </Layout>
      );
};

export default pelatihan;
